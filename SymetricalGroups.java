import java.util.ArrayList;


/**
 * Class to do some mathematical Operations on Symetrical Groups
 * including:
 * - Print Permutation in regular notation
 * - Print Permutation in cyclic notation
 * - Inverse Permutation
 * - Compose two different Permutations to a new one
 * @author Niclas Simmler
 * @version 1.1 / 15.3.2015
 */

public class SymetricalGroups {
	private int[][] permutation;
	private static ArrayList<Integer> sortHelper = new ArrayList<Integer>();
	
	/**
	 * Returns the Permutation of this Instance
	 * @return The Permutation as a two dimensional Array
	 */
	public int[][] getPermutation() {
		return permutation;
	}

	/**
	 * Returns a Helper Array which is being used internally for Sorting an Inversed Array
	 * Contains the indexes already used of a permutation
	 * @return The Helper Array of Indexes
	 */
	public static ArrayList<Integer> getSortHelper() {
		return sortHelper;
	}

	/**
	 * Main Class
	 */
	public static void main(String[] args) {
		SymetricalGroups group1 = new SymetricalGroups(4, new int[]{1,2,3,4}, new int[]{2,4,1,3});
		SymetricalGroups group2 = new SymetricalGroups(4, new int[]{1,2,3,4}, new int[]{3,2,1,4});
		//print(composition(group1.getPermutation(), group2.getPermutation()));
		//printCyclic(composition(group1.getPermutation(), group2.getPermutation()));

		SymetricalGroups group3 = new SymetricalGroups(4, new int[]{4,5,6,7}, new int[]{7,4,6,5});
		print(group3.getPermutation());
		print(inv(group3.getPermutation()));
	}
	
	/**
	 * Constructor
	 * Builds the Array representing the Permutation
	 * @param groupSize The Size of this Permutation
	 * @param upperRow The Upper Row of the Permutation
	 * @param lowerRow The Lower Row of the Permutation
	 */
	public SymetricalGroups(int groupSize, int[] upperRow, int[] lowerRow){
		if(upperRow.length == lowerRow.length){
			permutation = new int[2][groupSize];
			fillArray(0, upperRow);
			fillArray(1, lowerRow);
		}
	}
	
	/**
	 * Fills the Two Dimensional Array (Permutation)
	 * @param level Only {0,1} are accepted Values
	 * @param row The Row which should be filled into the Array
	 */
	private void fillArray(int level, int[] row){
		int i = 0;
		for (int element : row) {
			permutation[level][i] = element;
			i++;
		}
	}
	
	/**
	 * Inverses a Permutation
	 * @param permutationToInv The Permutation which should be inversed
	 * @return A new two dimensional Array representing the inversed Permutation
	 */
	public static int[][] inv(int[][] permutationToInv){
		int x = permutationToInv.length;
		int y = permutationToInv[0].length;
		int[][] tempInvPermutation = new int[x][y];
		
		for(int i = 0; i < y; i++){
			tempInvPermutation[0][i] = permutationToInv[1][i];
		}
		for(int i = 0; i < y; i++){
			tempInvPermutation[1][i] = permutationToInv[0][i];
		}
		return sortPermutation(tempInvPermutation);
	}
	
	/**
	 * Sorts any given Permutation in ascending Order (Row 0)
	 * @param permutation The Permutation to be sorted
	 * @return The Sorted Permutation in ascending Order
	 */
	private static int[][] sortPermutation(int[][] permutation){
		findOrderOfAscendingIndexes(permutation[0]);
		int[][] tempPermutation = new int[permutation.length][permutation[0].length];
		for(int i = 0; i < permutation[0].length; i++){
			tempPermutation[0][i] = permutation[0][getSortHelper().get(i)];
			tempPermutation[1][i] = permutation[1][getSortHelper().get(i)];
		}
		return tempPermutation;
	}
	
	/**
	 * Finds the correct Order of Indexes in ascending Order (3,1,2) would return (1,2,3)
	 * This function is usually being used internally
	 * @param array The array containing the values (normally the first row of a Permutation)
	 */
	private static void findOrderOfAscendingIndexes(int[] array){
		int smallest = Integer.MAX_VALUE;
		int smallestIndex = array.length;
		
		while(array.length != getSortHelper().size()){
			for(int i = 0; i < array.length; i++){
				if(array[i] < smallest){
					if(getSortHelper().contains(i)){
						continue;
					}else{
						smallest = array[i];
						smallestIndex = i;
					}
				}
			}
			getSortHelper().add(smallestIndex);
			smallest = Integer.MAX_VALUE;
			smallestIndex = array.length;
		}
	}
	
	/**
	 * Composes two Permutations to a new one (Remember the second Permutation is being run through first)
	 * @param f Representing the outer Permutation (left side of the compsition)
	 * @param g Represenitng the inner Permutation (right side of the composition, aka the one which is being run through first)
	 * @return A new two dimensional Array representing the composed Permutation
	 */
	public static int[][] composition(int[][] f, int[][] g){
		int fx = f.length;
		int fy = f[0].length;
		int gx = g.length;
		int gy = g[0].length;
		if(fx == gx && fy == gy){
			int[][] composition = new int[fx][fy];
			for(int i = 0; i < gy; i++){
				int currentValue = g[1][i];
				composition[0][i] = g[0][i];
				int indexOfFValue = -1;
				for(int j = 0; j < fy; j++){
					if(f[0][j] == currentValue){
						indexOfFValue = j;
					}
				}
				composition[1][i] = f[1][indexOfFValue];
			}
			return composition;
		}else{
			return null;
		}
	}
	
	/**
	 * Prints the cyclic notation of any given permutation
	 * @param permutation The two dimensional Array of the Permutation to be printed
	 */
	public static void printCyclic(int[][] permutation){
		String tempCycle = "";
		ArrayList<Integer> usedIndexes = new ArrayList<Integer>();
		ArrayList<String> printedCycles = new ArrayList<String>();
		for(int i = 0; i < permutation[0].length; i++){
			if(usedIndexes.contains(i)){
				continue;
			}
			if(permutation[0][i] == permutation[1][i]){
				System.out.print("("+permutation[0][i]+")");
				usedIndexes.add(i);
			}else{
				int startValue = permutation[0][i];
				int tempEndValue = permutation[1][i];
				tempCycle = tempCycle + "(" +startValue + ",";
				usedIndexes.add(i);
				while(startValue != tempEndValue){
					tempCycle = tempCycle + tempEndValue + ",";
					for(int j = 0; j < permutation[0].length; j++){
						if(permutation[0][j] == tempEndValue){
							tempEndValue = permutation[1][j];
							usedIndexes.add(j);
						}
					}
				}
				tempCycle = tempCycle.substring(0, tempCycle.length()-1);
				tempCycle = tempCycle + ")";
			}
			if(!(printedCycles.contains(tempCycle))){
				System.out.print(tempCycle);
			}
			printedCycles.add(tempCycle);
			if(usedIndexes.size() == permutation[0].length){
				break;
			}
		}
		System.out.println("\n");
	}
	
	/**
	 * Prints the regular notation of any given permutation
	 * @param permutation The two dimensional Array of the Permutation to be printed
	 */
	public static void print(int[][] permutation){
		for(int i = 0; i < permutation.length; i++){
			System.out.print("[");
			for(int j = 0; j < permutation[i].length; j++){
				System.out.print(" "+permutation[i][j]+" ");
			}
			System.out.println("]\n");
		}
	}
}