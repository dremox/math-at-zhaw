/**
 * Pow.java
 * Diese Klasse berechnet die Potenzmenge einer Menge von Zahlen
 * Autor: Niclas Simmler
 * Datum: 22.10.2014
 */
import java.util.*;
public class pow {
	public static void main(String[] args) {
		new pow("[1],[3]");
		new pow("[]");
		new pow("[[]]");
	}
	
	/**
	 * pow
	 * Konstruktor der Klasse und zugleich die Logik der Klasse
	 * mySet ist der String von Zahlen die die Menge modellieren
	 * myPowerSet ist die resultierende Potenzmenge
	 * myTempPowerSet ist eine temporäre Liste in die zuerst die Werte von mySet gespeichert werden
	 * 	danach werden diese der Liste myPowerSet hinzugefuegt
	 * Logik: 	1. Jedes Element aus myTempPowerSet wird der Liste myPowerSet hinzugefuegt
	 * 			2. Das hinzugefuegte Element wird mit jedem Element (ausser das gerade eben hinzugefuegte)
	 * 			   der Liste myPowerSet "mulzipliziert"
	 * 			3. Es wird die Leere Menge hinzugefuegt
	 * @param set Die übergebenen Werte die die Menge darstellen
	 * 		Formatierung der Menge: Eckige Klammern stellen ein Element einer Menge dar
	 */
	public pow(String set){
		String mySet = "[],[1],[2]"; //Mengendefinition
		ArrayList<String> myPowerSet = new ArrayList<String>();
		ArrayList<String> myTempPowerSet = new ArrayList<String>();
		System.out.println(mySet);
		for(String i : mySet.split(",")){
			myTempPowerSet.add(i.substring(i.indexOf("[")+1, i.length()-1));
		}
		int myTempPowerSetSize = myTempPowerSet.size();
		for(int i = 0; i < myTempPowerSetSize; i++){
			if(!(myPowerSet.contains(myTempPowerSet.get(i)))){
				myPowerSet.add(myTempPowerSet.get(i));
			}
			int myPowerSetSize = myPowerSet.size();
			for(int j = 0; j < myPowerSetSize; j++){
				if(!(myPowerSet.get(j).equals(myTempPowerSet.get(i)))){
					myPowerSet.add(myPowerSet.get(j)+","+myTempPowerSet.get(i));
				}
			}
		}
		if(!(myPowerSet.contains(""))){
			myPowerSet.add("");
		}
		int b = 0;
		for(String a : myPowerSet){
			b++;
			System.out.println(b+":"+a);
		}
	}
}